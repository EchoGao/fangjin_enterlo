<?php
namespace app\common\model;
class FangBuildingDesc extends \think\Model
{
    protected $resultSetType = 'collection';
    // 设置当前模型的数据库连接
    protected $connection = 'db_config_fang';
    protected $table = 'think_building_desc';
}