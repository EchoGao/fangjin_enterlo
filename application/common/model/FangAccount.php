<?php
namespace app\common\model;
class FangAccount extends \think\Model
{
    protected $resultSetType = 'collection';
    // 设置当前模型的数据库连接
    protected $connection = 'db_config_fang';
    protected $table = 'think_account';

    //所属上级
    public function higher()
    {
        return $this->hasOne('fangRecord','b_id','id');
    }

}