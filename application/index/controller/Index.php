<?php
namespace app\index\controller;
class Index extends \app\index\controller\Pre
{
    //首页？
    public function index()
    {
        $from = $this -> search_word_from();
        if($from['from'] || $from['keyword']){
            cookie('words',['words'=>$from['keyword'],'origin'=>$from['from']],24*3600);
        }
        if(cookie('words')){
            $cook =  cookie('words');
            $this -> assign('words',$cook['words']);
            $this -> assign('origin',$cook['origin']);
        }
        //展示当前所有的优选楼盘
        $allList = \app\common\model\FangBuildingList::column('list,building_id');
        $map[] = ['list','=',1];
        $exp = new \think\db\Expression("field(id,$allList[1])");
        $result = \app\common\model\FangBuilding::where($map)->order($exp) -> select();


        foreach ($result as $key => $value){
            $img = $value['design_img'];
            $result[$key]['design_img'] = self::$url.$img;
        }
        //展示当前所有高端楼盘
        $map1[] = ['list','=',2];
        $exp = new \think\db\Expression("field(id,$allList[2])");
        $result1 = \app\common\model\FangBuilding::where($map1)->order($exp)  -> select();
        foreach ($result1 as $key1 => $value1){
            $img1 = $value1['design_img'];
            $result1[$key1]['design_img'] = self::$url.$img1;
        }
        //展示所有商业地产
        $map2[] = ['list','=',3];
        $exp = new \think\db\Expression("field(id,$allList[3])");
        $result2 = \app\common\model\FangBuilding::where($map2)->order($exp)  -> select();
        foreach ($result2 as $key2 => $value2){
            $img2 = $value2['design_img'];
            $result2[$key2]['design_img'] = self::$url.$img2;
        }
        $this -> assign('info',$result);
        $this -> assign('info1',$result1);
        $this -> assign('info2',$result2);

        //传递预约数量
        $reportNum = \app\common\model\FangReport::where('date','>=',strtotime('2018-9-19'))->count();
        $this -> assign('reportNum',$reportNum+18);

        if (request()->isMobile()){
            return view();
        }
        else{
            return view('indexPC');
        }
    }
    static $url = 'https://show.enterlo.com';
    //详情页
    public function single()
    {
        $from = $this -> search_word_from();
        if($from['from'] || $from['keyword']){
            cookie('words',['words'=>$from['keyword'],'origin'=>$from['from']],24*3600);
        }
        if(cookie('words')){
            $cook =  cookie('words');
            $this -> assign('words',$cook['words']);
            $this -> assign('origin',$cook['origin']);
        }
        $data = input('param.');
        $info = \app\common\model\FangBuilding::with('buildingDesc')->where('id',$data['id'])->find();
        $type = explode(',',$info['type']);
        if (!$info)
            $this -> redirect('index/index');

        //获取经纪人信息
        if ($data['account_id']){
            $accountInfo = \app\common\model\FangAccount::where('id',$data['account_id'])->find();
            //如果是全民经纪人,展示管理者的信息
            if ($accountInfo['role'] =='33'){
                $higher = $accountInfo -> higher -> a_id;
                $higherInfo = \app\common\model\FangAccount::where('id',$higher)->find();
                $accountInfo['telephone'] = $higherInfo['telephone'];
            }
            $this -> assign('accountInfo',$accountInfo);
        }



        if ($type){
            $info = $info ->toArray();
            $buildingDesc = [];
            foreach ($type as $value){
                foreach ($info['building_desc'] as $k=> $item){
                    if ($value ==$item['id']){
                        $buildingDesc[] = $item;
                        unset($info['building_desc'][$k]);
                        break;
                    }
                }
            }
            foreach ($buildingDesc as $k => $value){
                $imgPre = explode(',',$value['img']);
                foreach ($imgPre as$j=> $item){
                    if ($item)
                        $imgPre[$j] = self::$url.$item;
                }
                $buildingDesc[$k]['img'] =$imgPre;
                if ($value['role'] =='video')
                    $buildingDesc[$k]['content'] = self::$url.$value['content'];
            }
            $info['building_desc'] = $buildingDesc;
        }

        $info['design_img'] = self::$url.$info['design_img'];
        $info['back_img'] = $info['back_img']?self::$url.$info['back_img']:null;
        $this -> assign('info',$info);

        //传递预约数量
        $reportNum = \app\common\model\FangReport::where('date','>=',strtotime('2018-9-19'))->count();
        $this -> assign('reportNum',$reportNum+18);

        if (request()->isMobile()){
            return view();
        }
        else{
            return view('singlePC');
        }
    }
    //搜索关键字
    function search_word_from() {
        $referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
        if(strstr( $referer, 'baidu.com')){ //百度
            preg_match( "|baidu.+wo?r?d=([^\\&]*)|is", $referer, $tmp );
            $keyword = urldecode( $tmp[1] );
            $from = 'baidu';
        }elseif(strstr( $referer, 'google.com') or strstr( $referer, 'google.cn')){ //谷歌
            preg_match( "|google.+q=([^\\&]*)|is", $referer, $tmp );
            $keyword = urldecode( $tmp[1] );
            $from = 'google';
        }elseif(strstr( $referer, 'so.com')){ //360搜索
            preg_match( "|so.+q=([^\\&]*)|is", $referer, $tmp );
            $keyword = urldecode( $tmp[1] );
            $from = '360';
        }elseif(strstr( $referer, 'sogou.com')){ //搜狗
            preg_match( "|sogou.com.+query=([^\\&]*)|is", $referer, $tmp );
            $keyword = urldecode( $tmp[1] );
            $from = 'sogou';
        }elseif(strstr( $referer, 'soso.com')){ //搜搜
            preg_match( "|soso.com.+w=([^\\&]*)|is", $referer, $tmp );
            $keyword = urldecode( $tmp[1] );
            $from = 'soso';
        }else {
            $keyword ='';
            $from = '';
        }
        return array('keyword'=>$keyword,'from'=>$from);
    }

}
