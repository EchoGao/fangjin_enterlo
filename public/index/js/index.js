$(function(){
    var words = $('body').data('words');
    var origin = $('body').data('origin');
    closeLayer();
    asideButtonEvent();
    phoneHave();
    upload1(words,origin);
    pLength();
    showMore();
    main_2_style();
});
function uploadData(text,tel,a,b){
    text == ''?alert('姓名不能为空'):(tel == ''?alert('电话号码不能为空'):
            $.post('https://show.enterlo.com/index/report/inserts',{
                name:text,
                telephone:tel,
                building:'房金时代首页',
                words:a,
                origin:b
            },function(res){
                alert(res.message)
            })
    )
}
function closeLayer(){
    var close = $('.layer-aside .layer-mask > div b');
    close.click(function(){
        $(this).parents('.layer-mask').fadeOut()
    })
}
function asideButtonEvent(){
    var button1 = $('.layer-aside .aside1');
    var button2 = $('.layer-aside .aside2');
    button1.click(function(){
        console.log('button1')
    })
    button2.click(function(){
        $('.layer-mask').fadeIn();
    })
}
function phoneHave(){
    var button = $('.layer-button > a.button2');
    var href = button.data('href');
    // var x= /^400-([0-9]){1}([0-9-]{7})$/.test(href.split(':')[1]);//400无转接正则判定
    // var y = /^400-([0-9]){1}([0-9-]{7})\,([0-9]){4}$/.test(href.split(':')[1]);//400有转接判定
    if(href.split(':')[1] !=''){
        button.attr('href',href);
    }
}
function upload1(a,b){
    // var button1 = $('footer .form input[type="button"]');
    var button2 = $('.layer-aside .layer-mask > div div.data input[type="button"]');
    button2.click(function(){
        var text = $('.layer-aside .layer-mask > div div.data input[type="text"]').val();
        var tel = $('.layer-aside .layer-mask > div div.data input[type="tel"]').val();
        uploadData(text,tel,a,b);
    })
}
function pLength(){
    var p = $('main>div>div .text pre');
    p.each(function(index){
        if(index == 0){
            if($(this).text().length >= 39){
                $(this).text($(this).text().slice(0,42)+'...');
            }
        }else if(index == 1){
            if($(this).text().length >= 69){
                $(this).text($(this).text().slice(0,72)+'...');
            }
        }else{
            if($(this).text().length >= 49){
                $(this).text($(this).text().slice(0,52)+'...');
            }
        }

    })
}
function showMore(){
    var button1 = $('#pr button');
    var button2 = $('#hr button');
    var button3 = $('#cr button');
    $('#pr>div').each(function(index){
        if(index<2){
            $(this).show();
            button1.hide()
        }else{
            button1.html('<span>展示更多 MORE</span><i></i>').css({
                fontSize:'14px',
                color:'#666'
            }).show();
        }
    })
    $('#hr>div').each(function(index){
        if(index<2){
            // console.log(index)
            $(this).show();
            button2.hide()
        }else{
            button2.html('<span>展示更多 MORE</span><i></i>').css({
                fontSize:'14px',
                color:'#666'
            }).show();
        }
    })
    $('#cr>div').each(function(index){
        if(index<2){
            // console.log(index)
            $(this).show();
            button3.hide()
        }else{
            button3.html('<span>展示更多 MORE</span><i></i>').css({
                fontSize:'14px',
                color:'#666'
            }).show();
        }
    })
    button1.click(function(){
        $(this).hide()
        $('#pr>div').show();
    })
    button2.click(function(){
        $(this).hide()
        $('#hr>div').show();
    })
    button3.click(function(){
        $(this).hide()
        $('#cr>div').show();
    })
}
function main_2_style(){
    var text = $('#hr .text');
    text.each(function(index){
        if((index+1)%2 == 0){
            $(this).css({
                right:'8%',
                left:'auto'
            })
        }else{
            $(this).css({
                left:'8%',
                right:'auto'
            })
        }
    })
}