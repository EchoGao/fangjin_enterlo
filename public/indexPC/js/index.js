$(function(){
    var words = $('body').data('words');
    var origin = $('body').data('origin');
    asideEvent();
    main_2_style();
    loginLayerEvent();
    footerEvent();
    upload(words,origin);
    pLength();
    showMore();
    topNav();
    $(window).scroll(function(){
        var header = $('header');
        header.css({
            border:0,
            boxShadow:'0 0 6px #999'
        });
        if($(window).scrollTop() == 0){
            header.css({
                'borderBottom':'3px solid #c4ab7a',
                'boxShadow':'none',

            })
        }
        if($(window).scrollTop() >= $('#pr').offset().top-50){
            // console.log($(window).scrollTop(),$('#pr').offset().top)
            $('header ul li').eq(0).addClass('color').siblings().removeClass('color');
        }
        if($(window).scrollTop() >= $('#hr').offset().top-50){
            $('header ul li').eq(1).addClass('color').siblings().removeClass('color');
        }
        if($(window).scrollTop() >= $('#cr').offset().top-50){
            $('header ul li').eq(2).addClass('color').siblings().removeClass('color');
        }
    })
});
function topNav(){
    $('header ul li').each(function(index){
        $(this).click(function(){
            $(this).addClass('color').siblings().removeClass('color');
            switch(index){
                case 0:
                    $('html,body').animate({
                        scrollTop:$('#pr').offset().top-50
                    });
                    break;
                case 1:
                    $('html,body').animate({
                        scrollTop:$('#hr').offset().top-50
                    });
                    break;
                case 2:
                    $('html,body').animate({
                        scrollTop:$('#cr').offset().top-50
                    });
                    break;
            }
        })

    })
    $('.banner ul li').each(function(index){
        $(this).click(function(){
            $(this).addClass('color').siblings().removeClass('color');
            switch(index){
                case 0:
                    $('html,body').animate({
                        scrollTop:$('#pr').offset().top-50
                    });
                    break;
                case 1:
                    $('html,body').animate({
                        scrollTop:$('#hr').offset().top-50
                    });
                    break;
                case 2:
                    $('html,body').animate({
                        scrollTop:$('#cr').offset().top-50
                    });
                    break;
            }
        })

    })
}
function asideEvent(){
    var li = $('aside ul li');
    li.hover(function(){
        var img = $(this).find('i').data('url1');
        $(this).find('div').animate({
            left:'-150px',
        }).show();
        $(this).css('backgroundColor','#c4ab7a');
        $(this).find('i').css('backgroundImage','url('+img+')');
    },function(){
        li.css('backgroundColor','#fff');
        li.find('i').each(function(){
            $(this).css('backgroundImage','url('+$(this).data('url2')+')');
            $(this).next().css({
                left:'-180px',
            }).hide();
        })
    });
    //滚动显示样式
    $('aside ul li.top').hide();
    $(window).scroll(function(){
        if($(this).scrollTop() > 450){
            $('aside ul li.top').show();
        }else{
            $('aside ul li.top').hide();
        }
    })
    /////////////
    //点击事件
    li.each(function(index){
        $(this).click(function(){
            switch(index){
                case 1:
                    $('.login-layer').fadeIn();
                    break;
                case 2:

                    break;
                case 3:
                    $("html,body").animate({scrollTop:0}, 500);
                    break;
            }
        })
    })
}
function loginLayerEvent(){
    var layer = $('.login-layer');
    var del = $('.login-layer .main .del');
    layer.click(function(event){
        if($(this)[0] == event.target){
            $(this).fadeOut();
        }
    })
    del.click(function(){
        layer.fadeOut();
    })
}
//联系我们按钮点击事件
function footerEvent(){
    var button2 = $('footer ul > li .button .last');
    button2.click(function(){
        $('.login-layer').fadeIn();
    })
}
//上传提交事件
function upload(a,b){
    var button1 = $('main .top .right .form input[type="button"]');
    var button2 = $('.login-layer .main .right button');
    button1.click(function(){
        var input1 = $('main .top .right .form input[type="text"]').val();
        var input2 = $('main .top .right .form input[type="tel"]').val();
        $.post('https://show.enterlo.com/index/report/inserts',{
            name:input1,
            telephone:input2,
            building:'房金时代首页',
            words:a,
            origin:b
        },function(res){
            hint(res.message);
        })
    })
    button2.click(function(){
        var input1 = $('.login-layer .main .right > div input[type="text"]').val();
        var input2 = $('.login-layer .main .right > div input[type="tel"]').val();
        $.post('https://show.enterlo.com/index/report/inserts',{
            name:input1,
            telephone:input2,
            building:'房金时代首页',
            words:a,
            origin:b
        },function(res){
            hint(res.message);
        })
    })
}
function pLength(){
    var p = $('main>div>div .text p');
    p.each(function(index){
        if($(this).text().length >= 250){
            $(this).text($(this).text().slice(0,252)+'...');
        }

    })
}
function showMore(){
    var button1 = $('#pr button');
    var button2 = $('#hr button');
    var button3 = $('#cr button');
    $('#pr>div').each(function(index){
        if(index<2){
            $(this).show();
            button1.hide()
        }else{
            button1.html('<span>展示更多 MORE</span><i></i>').css({
                fontSize:'14px',
                color:'#666'
            }).show()
        }
    })
    $('#hr>div').each(function(index){
        if(index<2){
            // console.log(index)
            $(this).show();
            button2.hide()
        }else{
            button2.html('<span>展示更多 MORE</span><i></i>').css({
                fontSize:'14px',
                color:'#666'
            }).show()
        }
    })
    $('#cr>div').each(function(index){
        if(index<2){
            // console.log(index)
            $(this).show();
            button3.hide()
        }else{
            button3.html('<span>展示更多 MORE</span><i></i>').css({
                fontSize:'14px',
                color:'#666'
            }).show()
        }
    })
    button1.click(function(){
        $(this).hide()
        $('#pr>div').show();
    })
    button2.click(function(){
        $(this).hide()
        $('#hr>div').show();
    })
    button3.click(function(){
        $(this).hide()
        $('#cr>div').show();
    })
}
function main_2_style(){
    var text = $('#hr .text');
    text.each(function(index){
        if((index+1)%2 == 0){
            $(this).css({
                right:'8%',
                left:'auto'
            })
        }else{
            $(this).css({
                left:'8%',
                right:'auto'
            })
        }
    })
}
//普通信息提示
function hint(test, a) {
    var layer = $('.admin-mask-layer');
    $('.admin-mask-layer p').html(test);
    layer.fadeIn('100');
    var time = setInterval(function () {
        layer.fadeOut('100');
        clearInterval(time);
        if (a) {
            a();
        }
    }, 2000);

}