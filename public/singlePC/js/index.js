$(function(){
    var words = $('body').data('words');
    var origin = $('body').data('origin');
    bgStyle();
    asideEvent();
    loginLayerEvent();
    footerEvent();
    upload(words,origin);
    headerClick();
    var accountphone = $('body').data('accountphone');
    if(accountphone){
        $('.phone-400').html(accountphone)
    }
});
function headerClick(){
    var logo = $('header .img');
    logo.click(function(){
        window.location.href = '/index/index/';
    })
}
function asideEvent(){
    var li = $('aside ul li');
    li.hover(function(){
        var img = $(this).find('i').data('url1');
        $(this).find('div').animate({
            left:'-150px',
        }).show();
        $(this).css('backgroundColor','#BEA770');
        $(this).find('i').css('backgroundImage','url('+img+')')
    },function(){
       li.css('backgroundColor','#fff');
       li.find('i').each(function(){
           $(this).css('backgroundImage','url('+$(this).data('url2')+')');
           $(this).next().css({
               left:'-180px',
           }).hide();
       })
    });
    //滚动显示样式
    $('aside ul li.top').hide();
    $(window).scroll(function(){
        if($(this).scrollTop() > 450){
            $('aside ul li.top').show();
        }else{
            $('aside ul li.top').hide();
        }
    })
    /////////////
    //点击事件
    li.each(function(index){
        $(this).click(function(){
            switch(index){
                case 1:
                 $('.login-layer').fadeIn();
                break;
                case 2:

                break;
                case 3:
                    $("html,body").animate({scrollTop:0}, 500);
                break;
            }
        })
    })
}
function loginLayerEvent(){
    var layer = $('.login-layer');
    var del = $('.login-layer .main .del');
    layer.click(function(event){
        if($(this)[0] == event.target){
           $(this).fadeOut();
        }
    })
    del.click(function(){
        layer.fadeOut();
    })
}
//联系我们按钮点击事件
function footerEvent(){
    // var button1 = $('footer ul > li .button .first');
    var button2 = $('footer ul > li .button .last');
    // button1.click(function(){
    //     window.open('http://put.zoosnet.net/LR/Chatpre.aspx?id=PUT46411304&lng=cn')
    // })
    button2.click(function(){
        $('.login-layer').fadeIn();
    })
}
//上传提交事件
function upload(a,b){
    var button1 = $('main .top .right .form input[type="button"]');
    var button2 = $('.login-layer .main .right button');
    button1.click(function(){
        var input1 = $('main .top .right .form input[type="text"]').val();
        var input2 = $('main .top .right .form input[type="tel"]').val();
        var accountid = $('body').data('accountid');
        if(accountid){
            $.post('https://show.enterlo.com/index/report/inserts',{
                name:input1,
                telephone:input2,
                building:$('body').data('building'),
                account_id:accountid,
                words:a,
                origin:b
            },function(res){
                if(res.code === "1"){
                    $('body').append('<iframe src="/index/index/succeed" style="display: none"></iframe>');
                    var time = setInterval(function(){
                        $('#iframe').remove();
                        clearInterval(time);
                    },3000);
                    $('.login-layer').fadeOut();
                }
                hint(res.message);
            })
        }else{
            $.post('https://show.enterlo.com/index/report/inserts',{
                name:input1,
                telephone:input2,
                building:$('body').data('building'),
                words:a,
                origin:b
            },function(res){
                if(res.code === "1"){
                    $('body').append('<iframe src="/index/index/succeed" style="display: none"></iframe>');
                    var time = setInterval(function(){
                        $('#iframe').remove();
                        clearInterval(time);
                    },3000);
                    $('.login-layer').fadeOut();
                }
                hint(res.message);
            })
        }    })
    button2.click(function(){
        var input1 = $('.login-layer .main .right > div input[type="text"]').val();
        var input2 = $('.login-layer .main .right > div input[type="tel"]').val();
        var accountid = $('body').data('accountid');
        if(accountid){
            $.post('https://show.enterlo.com/index/report/inserts',{
                name:input1,
                telephone:input2,
                building:$('body').data('building'),
                account_id:accountid,
                words:a,
                origin:b
            },function(res){
                if(res.code === "1"){
                    $('body').append('<iframe src="/index/index/succeed" style="display: none"></iframe>');
                    var time = setInterval(function(){
                        $('#iframe').remove();
                        clearInterval(time);
                    },3000);
                    $('.login-layer').fadeOut();
                }
                hint(res.message);
            })
        }else{
            $.post('https://show.enterlo.com/index/report/inserts',{
                name:input1,
                telephone:input2,
                building:$('body').data('building'),
                words:a,
                origin:b
            },function(res){
                if(res.code === "1"){
                    $('body').append('<iframe src="/index/index/succeed" style="display: none"></iframe>');
                    var time = setInterval(function(){
                        $('#iframe').remove();
                        clearInterval(time);
                    },3000);
                    $('.login-layer').fadeOut();
                }
                hint(res.message);
            })
        }
    })
}
//普通信息提示
function hint(test, a) {
    var layer = $('.admin-mask-layer');
    $('.admin-mask-layer p').html(test);
    layer.fadeIn('100');
    var time = setInterval(function () {
        layer.fadeOut('100');
        clearInterval(time);
        if (a) {
            a();
        }
    }, 2000);

}
function bgStyle(){
    var disc = $('main section .vimg>div');
    disc.each(function(index){
        if((index+1)%2 == 0){
            $(this).css({
                // backgroundColor:'#fff',
                'backgroundImage':'url(/singlePC/images/PC_top_background.svg)',
                'backgroundRepeat':'no-repeat',
                'backgroundPositionX':'100%',
                'backgroundSize':'contain'
            });
        }else{
            $(this).css({
                backgroundColor:'#f1f1f1',
            })
        }
    })
}