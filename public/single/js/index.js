$(function(){
    var words = $('body').data('words');
    var origin = $('body').data('origin');
    upload1(words,origin);
    bgShow();
    animateAll();
    closeLayer();
    asideButtonEvent();
    phoneHave();
    logoClick();
    $(window).scroll(function(){
        animateAll()
    });
    setTimeout(function(){
        $('.layer-aside .layer-mask').fadeIn();
    },5000)
});
function logoClick(){
    var logo = $('header .img');
    logo.click(function(){
        window.location.href = '/index/index';
    })
}
function bgShow(){
    // if($('#bg').css('background-image') !='none'){
    //     $('footer').css('background','none');
    //     $('footer i').hide();
    // }
}
function upload1(a,b){
    var button1 = $('footer .form input[type="button"]');
    var button2 = $('.layer-aside .layer-mask > div div.data input[type="button"]');
    button1.click(function(){
        var text = $('footer .form input[type="text"]').val();
        var tel = $('footer .form input[type="tel"]').val();
        uploadData(text,tel,a,b);
    })
    button2.click(function(){
        var text = $('.layer-aside .layer-mask > div div.data input[type="text"]').val();
        var tel = $('.layer-aside .layer-mask > div div.data input[type="tel"]').val();
        uploadData(text,tel,a,b);
    })
}
function animateAll(){
    $('.details>div,footer').each(function(){
    if($(window).scrollTop() + $(window).height() - $(this).offset().top >= 0){
        $(this).addClass('animate');
    }
    if($(window).scrollTop() + $(window).height() - $('footer>div.animation').offset().top >= 20){
        $('footer>div.animation').addClass('animate');
    }
});

}
function uploadData(text,tel,a,b){
    var accountid = $('body').data('accountid');
    text == ''?alert('姓名不能为空'):(tel == ''?alert('电话号码不能为空'):
            (accountid? $.post('https://show.enterlo.com/index/report/inserts',{
                    name:text,
                    telephone:tel,
                    building:$('body').data('building'),
                    account_id:accountid,
                    words:a,
                    origin:b
                },function(res){
                    if(res.code === "1"){
                        $('body').append('<iframe src="/index/index/succeed" id="iframe" style="display: none"></iframe>');
                        var time = setInterval(function(){
                            $('#iframe').remove();
                            clearInterval(time);
                        },3000);
                        $('.layer-mask').fadeOut();
                    }
                    alert(res.message)
                })
                :
                $.post('https://show.enterlo.com/index/report/inserts',{
                    name:text,
                    telephone:tel,
                    building:$('body').data('building'),
                    words:a,
                    origin:b
                },function(res){
                    if(res.code === "1"){
                        $('body').append('<iframe src="/index/index/succeed" id="iframe" style="display: none"></iframe>');
                        var time = setInterval(function(){
                            $('#iframe').remove();
                            clearInterval(time);
                        },3000);
                        $('.layer-mask').fadeOut();
                    }
                    alert(res.message)
                }))
    )
}
function closeLayer(){
    var close = $('.layer-aside .layer-mask > div b');
    close.click(function(){
        $(this).parents('.layer-mask').fadeOut()
    })
}
function asideButtonEvent(){
    var button1 = $('.layer-aside .aside1');
    var button2 = $('.layer-aside .aside2');
    button1.click(function(){
        console.log('button1')
    })
    button2.click(function(){
        $('.layer-mask').fadeIn();
    })
}
function phoneHave(){
    var button = $('.phone-400');
    var accountphone = $('body').data('accountphone');
    // var href = accountphone;
    // var x= /^400-([0-9]){1}([0-9-]{7})$/.test(href.split(':')[1]);//400无转接正则判定
    // var y = /^400-([0-9]){1}([0-9-]{7})\,([0-9]){4}$/.test(href.split(':')[1]);//400有转接判定
    if(accountphone){
        button.attr('href','tel:'+accountphone);
    }
}