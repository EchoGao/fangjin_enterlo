<?php
namespace wxpay;
use wxpay\lib\WxPayConfig;
class OpenID
{
    //设置网络请求配置
    public function _request($curl,$https=true,$method='GET',$data=null){
        // 创建一个新cURL资源
        $ch = curl_init();

        // 设置URL和相应的选项
        curl_setopt($ch, CURLOPT_URL, $curl);    //要访问的网站
        curl_setopt($ch, CURLOPT_HEADER, false);    //启用时会将头文件的信息作为数据流输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //将curl_exec()获取的信息以字符串返回，而不是直接输出。

        if($https){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  //FALSE 禁止 cURL 验证对等证书（peer's certificate）。
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  //验证主机
        }
        if($method == 'POST'){
            curl_setopt($ch, CURLOPT_POST, true);  //发送 POST 请求
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //全部数据使用HTTP协议中的 "POST" 操作来发送。
        }


        // 抓取URL并把它传递给浏览器
        $content = curl_exec($ch);
        if ($content  === false) {
            return "网络请求出错: " . curl_error($ch);
            exit();
        }
        //关闭cURL资源，并且释放系统资源
        curl_close($ch);

        return $content;
    }


    /**
     * 获取用户的openid
     * @param  string $openid [description]
     * @return [type]         [description]
     */
    public function baseAuth($redirect_url){


        //1.准备scope为snsapi_base网页授权页面
        $baseurl = urlencode($redirect_url);

        $snsapi_base_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.WxPayConfig::APPID.'&redirect_uri='.$baseurl.'&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';

        //2.静默授权,获取code
        //页面跳转至redirect_uri/?code=CODE&state=STATE
        $code = $_GET['code'];
        if( !isset($code) ){
            header('Location:'.$snsapi_base_url);
            exit;
        }


        //3.通过code换取网页授权access_token和openid
        $curl = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.WxPayConfig::APPID.'&secret='.WxPayConfig::APPSECRET.'&code='.$code.'&grant_type=authorization_code';
        $content = $this->_request($curl);
        $result = json_decode($content,true);

        //3.拉取用户信息(需scope为 snsapi_userinfo)
        $user_url = "https://api.weixin.qq.com/sns/userinfo?access_token=$result[access_token]&openid=$result[openid]&lang=zh_CN";
        $userInfoRe = $this->_request($user_url);
        $userInfo = json_decode($userInfoRe,true);
        return $userInfo;
    }

}